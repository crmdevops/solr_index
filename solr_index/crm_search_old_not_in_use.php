<?php 

set_time_limit(0);
error_reporting(E_ALL &~E_NOTICE &~E_WARNING &~E_DEPRECATED);
ini_set('memory_limit', '-1');
define('SOLR_SERVER_HOSTNAME','172.31.9.2');
define('SOLR_SERVER_USERNAME','');
define('SOLR_SERVER_PASSWORD','');
define('SOLR_SERVER_PORT','8080');
define('SOLR_SERVER_COLLECTION_NAME','solr/collection2');

//DB Configurations
$env_mode = 'LOCAL';
require 'database.php';

$options = array
(
    'hostname' => SOLR_SERVER_HOSTNAME,
    'login'    => SOLR_SERVER_USERNAME,
    'password' => SOLR_SERVER_PASSWORD,
    'port'     => SOLR_SERVER_PORT,
    'path' => SOLR_SERVER_COLLECTION_NAME
);

//Global Variables declaration
$totalRowsToIndex = 0;
$indexLimit = 500;

function commitAll(){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://'.SOLR_SERVER_HOSTNAME.':'.SOLR_SERVER_PORT.'/'.SOLR_SERVER_COLLECTION_NAME.'/update?commit=true');
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true); 
    $resp = curl_exec($ch);
    return $resp;
}

function convertToMultiValue($KVP,$separator=','){
    
}

function convertSolrCompatiableChars($string){
    if($string===0 || gettype($string)=='integer'){
       return $string; 
    }
    elseif(!empty($string)){
        //$string = preg_replace('@[\x00-\x08\x0B\x0C\x0E-\x1F]@', ' ', $string);
        return htmlspecialchars($string, ENT_NOQUOTES, 'UTF-8');
    }
    return null;
}

function deleteAllDocuments(){
    global $options;
    $client = new SolrClient($options);
    $res = $client->deleteByQuery("*:*");
    commitAll();
}

function deleteByUids($Uids){
    global $options;
    $client = new SolrClient($options);
    $res = false;
    try {
        if( !empty($Uids) && is_array($Uids)){
            $res = $client->deleteByIds($Uids);
        }
        else if(!empty($Uids)){
            $res = $client->deleteById($Uids);
        }
        if($res){
            commitAll();
            return true;
        }
    }
    catch (Exception $e){
        echo '\n'.$e->getMessage();
        return false;
    }
}
function AutoSuggest($prefix){
    global $options;
    $client = new SolrClient($options);
    $prefix = strtolower($prefix);
    $client->setResponseWriter("json");
    $query = new SolrQuery('*:*');
    $query->setFacet(true);
    $query->setRows(0);
    $query->addFacetField('CompanyName')->addFacetField('OpportunityName')->addFacetField('ContactName')->addFacetField('Subject')->setFacetMinCount(1)->setFacetPrefix($prefix);
    $updateResponse = $client->query($query);
    $response_array = $updateResponse->getResponse();
    $facet_data = $updateResponse->facet_counts->facet_fields;
    //print_r($facet_data);
    //$facet_data = str_replace(array("\n", "\r", "\t"), '', $facet_data);    // removes newlines, returns and tabs
    print_r($updateResponse);
    // replace double quotes with single quotes, to ensure the simple XML function can parse the XML
    //$facet_data = trim(str_replace('"', "'", $facet_data));
    //$simpleXml = simplexml_load_string($facet_data);

    //$xyz = stripslashes(json_encode($simpleXml));
    
}

function MainQuery($type,$value){
    global $options;
    $client = new SolrClient($options);
    //$type= 'Case';
    //$value = 'ABC';
    //'ContactName:\"'.$value.'\"'
    if($type=='Contact')
     {
        $query = new SolrQuery("ContactName:\"".$value."\"");
        $query->addFilterQuery('Data_Type:Contact');
        $query->addField('Data_Type')->addField('UID')->addField('ContactName')->addField('CName')->addField('LastViewedDate')->addField('Designation')->addField('Email')->addField('Phone');
     }
     if($type=='Record')
     {
        $query = new SolrQuery("CompanyName:\"".$value."\"");
        $query->addFilterQuery('Data_Type:Record');
        $query->addField('Data_Type')->addField('UID')->addField('CompanyName')->addField('City')->addField('LastViewedDate')->addField('ProfileName')->addField('RatingName')->addField('OwnerName')->addField('ConName');
     }
     if($type=='Opportunity')
     {
        $query = new SolrQuery("OpportunityName:\"".$value."\"");
        $query->addFilterQuery('Data_Type:Opportunity');
        $query->addField('Data_Type')->addField('UID')->addField('OpportunityName')->addField('CName')->addField('LastViewedDate')->addField('OpportunityValue')->addField('OpportunityStatusName');
     }
     if($type=='Case')
     {
        $query = new SolrQuery("Subject:\"".$value."\"");
        $query->addFilterQuery('Data_Type:Case');
        $query->addField('Data_Type')->addField('UID')->addField('Subject')->addField('CName')->addField('LastViewedDate')->addField('CreatedName')->addField('CaseStatus')->addField('PriorityName')->addField('AssignedName')->addField('DaysCaseOpened');
     }  
    
    $query->setStart(0);
    $query->setRows(50);
    $client->setResponseWriter("json");
    $query_response = $client->query($query);
    $response = $query_response->getResponse();
    print_r($query_response);
}
function addData($db){
    global $options;
    global $totalRowsToIndex, $indexLimit;
    $limit = $indexLimit;
    $close = false;
    $doc = $data = $uids = array();
    $database_name = $db;
    $con = mssql_connect(DB_HOST,DB_USER,DB_PASS);
    $link = mssql_select_db($database_name,$con);
    $k = 1;
    $sql_cnt = "SELECT count(1) as cnt from tbSearch_FlatTable WHERE Indexed = 0 ";
    $query_cnt = mssql_query($sql_cnt) or die('Query 2 failed: ');
    $cnt_result = mssql_fetch_assoc($query_cnt);
    $totalRowsToIndex = $cnt_result['cnt'];
    //$sql = "SELECT TOP {$limit} CAST(Designation AS TEXT) AS Designation,CAST(Email AS TEXT) AS Email,CAST(City AS TEXT) AS City,LastViewedDate,CAST(ProfileName AS TEXT) AS ProfileName,CAST(RatingName AS TEXT) AS RatingName,CAST(OwnerName AS TEXT) AS OwnerName,Id,CAST(UID AS TEXT) AS UID,CAST(SubscriptionId AS TEXT) AS SubscriptionId,CAST(Data_Type AS TEXT) AS Data_Type,UserId,CAST(Access_UserId AS TEXT) AS Access_UserId,CAST(CompanyName AS TEXT) AS CompanyName,CAST(ContactName AS TEXT) AS ContactName,CAST(Phone AS TEXT) AS Phone,CAST(OpportunityName AS TEXT) AS OpportunityName,CAST(OpportunityValue AS TEXT) AS OpportunityValue,CAST(OpportunityStatusName AS TEXT) AS OpportunityStatusName,ClosingDate,CAST(Subject AS TEXT) AS Subject,CAST(CreatedName AS TEXT) AS CreatedName,CAST(CaseStatus AS TEXT) AS CaseStatus,CAST(PriorityName AS TEXT) AS PriorityName,CAST(CaseAssignedTo AS TEXT) AS AssignedName,CaseCreatedDate,CAST(RecId AS TEXT) AS RecId,CAST(RecordType AS TEXT) AS RecordType from tbSearch_FlatTable WHERE Indexed = 0 order by Id asc";
    //$sql = "SELECT TOP {$limit} CAST(LeadStatus AS TEXT) AS LeadStatus,CAST(Mobile AS TEXT) AS Mobile,CAST(UID AS TEXT) AS UID,Cid,OpportuntityWon,CAST(Mobile AS TEXT) AS Mobile,CAST(CaseLinkedProduct AS TEXT) AS CaseLinkedProduct,CAST(DirectDial AS TEXT) AS DirectDial,CAST(Fax AS TEXT) AS Fax,CAST(OpportunityProbability AS TEXT) AS OpportunityProbability,CAST(OpportunityOwner AS TEXT) AS OpportunityOwner,CAST(Industry AS TEXT) AS Industry,CAST(OriginatedFrom AS TEXT) AS OriginatedFrom,CAST(ContactFrequency AS TEXT) AS ContactFrequency,CAST(LeadSource AS TEXT) AS LeadSource,CAST(JobLevel AS TEXT) AS JobLevel,CAST(JobFunction AS TEXT) AS JobFunction,CAST(ContactCountry AS TEXT) AS ContactCountry,CAST(CompanyCountry AS TEXT) AS CompanyCountry,CAST(CompanyZip AS TEXT) AS CompanyZip,CAST(ContactZip AS TEXT) AS ContactZip,CAST(CompanyState AS TEXT) AS CompanyState,CAST(ContactState AS TEXT) AS ContactState,CAST(CompanyStreet AS TEXT) AS CompanyStreet,CAST(ContactStreet AS TEXT) AS ContactStreet,CAST(CompanyBuilding AS TEXT) AS CompanyBuilding,CAST(ContactBuilding AS TEXT) AS ContactBuilding,CAST(AccountCode AS TEXT) AS AccountCode,CAST(Designation AS TEXT) AS Designation,CAST(Email AS TEXT) AS Email,CAST(CompanyCity AS TEXT) AS CompanyCity,CAST(ContactCity AS TEXT) AS ContactCity,CompanyLastViewDate,ContactLastViewDate,CAST(ProfileName AS TEXT) AS ProfileName,CAST(RatingName AS TEXT) AS RatingName,CAST(OwnerName AS TEXT) AS OwnerName,Id,CAST(UID AS TEXT) AS UID,CAST(SubscriptionId AS TEXT) AS SubscriptionId,CAST(Data_Type AS TEXT) AS Data_Type,UserId,CAST(Access_UserId AS TEXT) AS Access_UserId,CAST(CompanyName AS TEXT) AS CompanyName,CAST(ContactName AS TEXT) AS ContactName,CAST(Phone AS TEXT) AS Phone,CAST(OpportunityName AS TEXT) AS OpportunityName,CAST(OpportunityValue AS TEXT) AS OpportunityValue,CAST(OpportunityStatusName AS TEXT) AS OpportunityStatusName,OpportunityClosingDate,OpportunityCreateDate,CAST(Subject AS TEXT) AS Subject,CAST(CaseCreatedName AS TEXT) AS CaseCreatedName,CAST(CaseStatus AS TEXT) AS CaseStatus,CAST(CasePriority AS TEXT) AS CasePriority,CAST(CaseAssignedTo AS TEXT) AS CaseAssignedTo,CaseCreatedDate,CAST(RecId AS TEXT) AS RecId,CAST(RecordType AS TEXT) AS RecordType from tbSearch_FlatTable WHERE Indexed = 0 order by Id asc";
    
    $sql = "SELECT TOP {$limit} CAST(LeadStatus AS TEXT) AS LeadStatus,CAST(Mobile AS TEXT) AS Mobile,CAST(UID AS TEXT) AS UID,Cid,OpportuntityWon,CAST(Mobile AS TEXT) AS Mobile,CAST(CaseLinkedProduct AS TEXT) AS CaseLinkedProduct,CAST(DirectDial AS TEXT) AS DirectDial,CAST(Fax AS TEXT) AS Fax,CAST(OpportunityProbability AS TEXT) AS OpportunityProbability,CAST(OpportunityOwner AS TEXT) AS OpportunityOwner,CAST(Industry AS TEXT) AS Industry,CAST(OriginatedFrom AS TEXT) AS OriginatedFrom,CAST(ContactFrequency AS TEXT) AS ContactFrequency,CAST(LeadSource AS TEXT) AS LeadSource,CAST(JobLevel AS TEXT) AS JobLevel,CAST(JobFunction AS TEXT) AS JobFunction,CAST(ContactCountry AS TEXT) AS ContactCountry,CAST(CompanyCountry AS TEXT) AS CompanyCountry,CAST(CompanyZip AS TEXT) AS CompanyZip,CAST(ContactZip AS TEXT) AS ContactZip,CAST(CompanyState AS TEXT) AS CompanyState,CAST(ContactState AS TEXT) AS ContactState,CAST(CompanyStreet AS TEXT) AS CompanyStreet,CAST(ContactStreet AS TEXT) AS ContactStreet,CAST(CompanyBuilding AS TEXT) AS CompanyBuilding,CAST(ContactBuilding AS TEXT) AS ContactBuilding,CAST(AccountCode AS TEXT) AS AccountCode,CAST(Designation AS TEXT) AS Designation,CAST(Email AS TEXT) AS Email,CAST(CompanyCity AS TEXT) AS CompanyCity,CAST(ContactCity AS TEXT) AS ContactCity,CompanyLastViewDate,ContactLastViewDate,CAST(ProfileName AS TEXT) AS ProfileName,CAST(RatingName AS TEXT) AS RatingName,CAST(OwnerName AS TEXT) AS OwnerName,Id,CAST(UID AS TEXT) AS UID,CAST(SubscriptionId AS TEXT) AS SubscriptionId,CAST(Data_Type AS TEXT) AS Data_Type,UserId,CAST(CompanyName AS TEXT) AS CompanyName,CAST(ContactName AS TEXT) AS ContactName,CAST(Phone AS TEXT) AS Phone,CAST(OpportunityName AS TEXT) AS OpportunityName,CAST(OpportunityValue AS TEXT) AS OpportunityValue,CAST(OpportunityStatusName AS TEXT) AS OpportunityStatusName,OpportunityClosingDate,OpportunityCreateDate,CAST(Subject AS TEXT) AS Subject,CAST(CaseCreatedName AS TEXT) AS CaseCreatedName,CAST(CaseStatus AS TEXT) AS CaseStatus,CAST(CasePriority AS TEXT) AS CasePriority,CAST(CaseAssignedTo AS TEXT) AS CaseAssignedTo,CaseCreatedDate,CAST(RecId AS TEXT) AS RecId,CAST(RecordType AS TEXT) AS RecordType,CAST(TeamId AS TEXT) AS TeamId from tbSearch_FlatTable WHERE Cid IS NOT NULL AND Cid != 0 AND Indexed = 0 order by Id asc";
    $query = mssql_query($sql) or die(mssql_get_last_message());
    $uid = '';
    $updated_date = '';
    //Instantiating Solr Client
    $client = new SolrClient($options);
    while($result = mssql_fetch_assoc($query)){
	if($result['Cid'] == NULL || $result['Cid'] == 0){
		echo '   UID '.$result['UID']. ' does not have cid. '.$result['Cid']. '--';
		continue;
	}
        $results = array();
        $results = $result;
        $doc[$k] = new SolrInputDocument();   
        foreach($results as $key => $value){
            $value = trim($value);
            $i = 1;
            switch($key){
                case 'UID':
                    print $value;
                   $doc[$k]->addField('UID',$value);                   
                break;
                case 'RecId':
                case 'RecordType':
                case 'SubscriptionId':
                case 'Data_Type':
                case 'UserId':
                   if($value){
                      $doc[$k]->addField($key,$value); 
                   }                          
                break;
                case 'TeamId': 
                case 'Access_UserId':
                    /*if($value){
                        if(strpos($value,',')){
                          $DlAreas = explode(',', $value);
                          foreach($DlAreas as $DlArea){
                            $valM = trim($DlArea);
                            $data[$k][$key][] = $valM;
                            $doc[$k]->addField($key,$valM);
                          }
                        }
                        else{
                            $valM = $value;
                            $data[$k][$key][] = $valM;
                            $doc[$k]->addField($key,$valM);
                        }
                    }else{
                        $value = 0;
                        $doc[$k]->addField($key,$value);
                    }*/                 
                break;    
                /*case 'CompanyName':
                    if($value){
                        if($results['Data_Type'] == 'Record')
                        {
                          $key = 'CName';
                          $value = mb_convert_encoding($value, 'UTF-8', 'UTF-8');
                          $doc[$k]->addField($key,$value);
                          $key = 'CompanyName';
                        }
                        else
                        {
                          $key = 'CName';  
                        }
                        $value = mb_convert_encoding($value, 'UTF-8', 'UTF-8');  
                        $doc[$k]->addField($key,$value);
                    }                 
                break;*/
                case 'CompanyName':
                    if($value){
                        if($results['Data_Type'] == 'Record')
                        {
                          $key = 'CName';
                          $value = mb_convert_encoding($value, 'UTF-8', 'UTF-8');
                          $doc[$k]->addField($key,$value);
                          $key = 'CompanyName';
                          $value = mb_convert_encoding($value, 'UTF-8', 'UTF-8');  
                          $doc[$k]->addField($key,$value);
                        }
                    }                 
                break;
                case 'ContactName':
                   if($value){
                if($results['Data_Type'] == 'Contact')
                    {
                      $key = 'ConName';
                      $value = mb_convert_encoding($value, 'UTF-8', 'UTF-8');
                      $doc[$k]->addField($key,$value);
                      $key = 'ContactName';
                    }
                    else
                    {
                      $key = 'ConName';  
                    }
                    $value = mb_convert_encoding($value, 'UTF-8', 'UTF-8');   
                    $doc[$k]->addField($key,$value);
                   }                 
                break;
                case 'CompanyLastViewDate':
                case 'ContactLastViewDate':
                case 'OpportunityClosingDate':
                case 'OpportunityCreateDate':
                case 'CaseCreatedDate':
                    if($value){
                      $d1 = $d2 = $date = '';
                      $d1 = date('Y-m-d',(int) strtotime($value));
                      $d2 = date('H:i:s',(int) strtotime($value));
                      $date = $d1.'T'.$d2.'Z';
                      $data[$k][$key] = $date;
                      $doc[$k]->addField($key,$date);  
                    }
                break;
                case 'CompanyCity':
                case 'ContactCity':
                case 'ProfileName':
                case 'RatingName':
                case 'OwnerName':
                case 'Designation':
                case 'Email':
                case 'Phone':
                case 'OpportunityName':
                case 'OpportunityValue':
                case 'OpportunityStatusName':
                case 'Subject':
                case 'CaseCreatedName':
                case 'CaseStatus':
                case 'CasePriority':
                case 'CaseAssignedTo':
                case 'AccountCode':
                case 'ContactBuilding':
                case 'CompanyBuilding':
                case 'CompanyStreet':
                case 'ContactStreet':
                case 'CompanyState':
                case 'ContactState':
                case 'CompanyZip':
                case 'ContactZip':
                case 'ContactCountry':
                case 'CompanyCountry':
                case 'JobFunction':
                case 'JobLevel':
                case 'LeadSource':
                case 'ContactFrequency':
                case 'OriginatedFrom':
                case 'Industry':
                case 'OpportunityOwner':
                case 'OpportunityProbability':
                case 'Fax':
                case 'DirectDial':
                case 'Mobile':
                case 'CaseLinkedProduct':
                case 'OpportuntityWon':
                case 'LeadStatus':
                   if($value){
                    $value = mb_convert_encoding($value, 'UTF-8', 'UTF-8');
                    $doc[$k]->addField($key,$value);
                   }
                break;
                case 'Cid':
                case 'Indexed':
                break;
           }
    $uids[] = $result['Uid'];
    
    $updated_date = date('Y-m-d H:i:s');//$result['CreatedOn'];
    }
    $row_ids[] = $result['Id'];
    $date_indexed_d1 = date('Y-m-d');
    $date_indexed_d2 = date('H:i:s');
    $date_indexed = $date_indexed_d1.'T'.$date_indexed_d2.'Z';
    $doc[$k]->addField('date_indexed',$date_indexed);
    //print $doc['CompanyName'];
    //echo($result['UID']);
    if($result['Data_Type']!='Record')
       {
          $sql_cid = "SELECT CAST(LeadStatus AS TEXT) AS LeadStatus,CAST(CompanyName AS TEXT) AS CompanyName,CAST(CompanyCity AS TEXT) AS CompanyCity,CompanyLastViewDate,CAST(ProfileName AS TEXT) AS ProfileName,CAST(RatingName AS TEXT) AS RatingName,CAST(OwnerName AS TEXT) AS OwnerName,CAST(AccountCode AS TEXT) AS AccountCode,CAST(CompanyBuilding AS TEXT) AS CompanyBuilding,CAST(CompanyStreet AS TEXT) AS CompanyStreet,CAST(CompanyState AS TEXT) AS CompanyState,CAST(CompanyZip AS TEXT) AS CompanyZip,CAST(CompanyCountry AS TEXT) AS CompanyCountry,CAST(JobFunction AS TEXT) AS JobFunction,CAST(JobLevel AS TEXT) AS JobLevel,CAST(LeadSource AS TEXT) AS LeadSource,CAST(ContactFrequency AS TEXT) AS ContactFrequency,CAST(OriginatedFrom AS TEXT) AS OriginatedFrom,CAST(Industry AS TEXT) AS Industry,CAST(Fax AS TEXT) AS Fax from tbSearch_FlatTable WHERE Cid =".$result['Cid']." AND Data_Type='Record'";
          //echo $sql_cid;
          $query_cid = mssql_query($sql_cid) or die('Query 4 failed: ');
          //$cid_result = array();
          $cid_result = mssql_fetch_assoc($query_cid);
          //echo "<pre>"; print_r($cid_result); die;
          $cid_result['CompanyName'] = mb_convert_encoding($cid_result['CompanyName'], 'UTF-8', 'UTF-8');
          $doc[$k]->addField('CName',$cid_result['CompanyName']);
          $cid_result['CompanyCity'] = mb_convert_encoding($cid_result['CompanyCity'], 'UTF-8', 'UTF-8');
          $doc[$k]->addField('CompanyCity',$cid_result['CompanyCity']);
          if($cid_result['CompanyLastViewDate']!='NULL')
          {
             $d1 = $d2 = $date = '';
             $d1 = date('Y-m-d',(int) strtotime($cid_result['CompanyLastViewDate']));
             $d2 = date('H:i:s',(int) strtotime($cid_result['CompanyLastViewDate']));
             $date = $d1.'T'.$d2.'Z';
             $doc[$k]->addField('CompanyLastViewDate',$date);  
          }
          $cid_result['ProfileName'] = mb_convert_encoding($cid_result['ProfileName'], 'UTF-8', 'UTF-8');
          $doc[$k]->addField('ProfileName',$cid_result['ProfileName']);
          $cid_result['RatingName'] = mb_convert_encoding($cid_result['RatingName'], 'UTF-8', 'UTF-8');
          $doc[$k]->addField('RatingName',$cid_result['RatingName']);
          $cid_result['OwnerName'] = mb_convert_encoding($cid_result['OwnerName'], 'UTF-8', 'UTF-8');
          $doc[$k]->addField('OwnerName',$cid_result['OwnerName']);
          $cid_result['AccountCode'] = mb_convert_encoding($cid_result['AccountCode'], 'UTF-8', 'UTF-8');
          $doc[$k]->addField('AccountCode',$cid_result['AccountCode']);
          $cid_result['CompanyBuilding'] = mb_convert_encoding($cid_result['CompanyBuilding'], 'UTF-8', 'UTF-8');
          $doc[$k]->addField('CompanyBuilding',$cid_result['CompanyBuilding']);
          $cid_result['CompanyStreet'] = mb_convert_encoding($cid_result['CompanyStreet'], 'UTF-8', 'UTF-8');
          $doc[$k]->addField('CompanyStreet',$cid_result['CompanyStreet']);
          $cid_result['CompanyState'] = mb_convert_encoding($cid_result['CompanyState'], 'UTF-8', 'UTF-8');
          $doc[$k]->addField('CompanyState',$cid_result['CompanyState']);
          $cid_result['CompanyZip'] = mb_convert_encoding($cid_result['CompanyZip'], 'UTF-8', 'UTF-8');
          $doc[$k]->addField('CompanyZip',$cid_result['CompanyZip']);
          $cid_result['CompanyCountry'] = mb_convert_encoding($cid_result['CompanyCountry'], 'UTF-8', 'UTF-8');
          $doc[$k]->addField('CompanyCountry',$cid_result['CompanyCountry']);
          $cid_result['LeadSource'] = mb_convert_encoding($cid_result['LeadSource'], 'UTF-8', 'UTF-8');
          $doc[$k]->addField('LeadSource',$cid_result['LeadSource']);
          $cid_result['ContactFrequency'] = mb_convert_encoding($cid_result['ContactFrequency'], 'UTF-8', 'UTF-8');
          $doc[$k]->addField('ContactFrequency',$cid_result['ContactFrequency']);
          $cid_result['OriginatedFrom'] = mb_convert_encoding($cid_result['OriginatedFrom'], 'UTF-8', 'UTF-8');
          $doc[$k]->addField('OriginatedFrom',$cid_result['OriginatedFrom']);
          $cid_result['Industry'] = mb_convert_encoding($cid_result['Industry'], 'UTF-8', 'UTF-8');
          $doc[$k]->addField('Industry',$cid_result['Industry']);
          $cid_result['Fax'] = mb_convert_encoding($cid_result['Fax'], 'UTF-8', 'UTF-8');
          $doc[$k]->addField('Fax',$cid_result['Fax']);
          $cid_result['LeadStatus'] = mb_convert_encoding($cid_result['LeadStatus'], 'UTF-8', 'UTF-8');
          $doc[$k]->addField('LeadStatus',$cid_result['LeadStatus']);
          //echo "1111<pre>"; print_r($cid_result); die;
       }
    $k++;
  }

  if($k>1){
      try{
        //echo "yes";
        $response = $client->addDocuments($doc);
        
        $start_row_id = $row_ids[0];
        $last_row_id = end($row_ids);
                $rowid_str = implode(',',$row_ids); 
        if(commitAll()){
            
           $index_master_update = "UPDATE tbSearch_FlatTable set Indexed = 1, CreatedOn = '".$updated_date."' where Id IN(".$rowid_str .")";
           mssql_query($index_master_update);        
           unset($client);
           echo"\n".($k-1). " rows between ".$start_row_id." and ".$last_row_id."  uploaded and Commited SuccessFully.";
           //header("Location:".$rowid_str);
           //exit;

           if($totalRowsToIndex>$indexLimit){
                if( ($totalRowsToIndex-$indexLimit)< $indexLimit){
                    $indexLimit = $totalRowsToIndex-$indexLimit;
                }
                echo "\n still ".($totalRowsToIndex-$indexLimit)." rows more left to update";
                sleep(1);
                return addData($db); 
           }
        } 
      }
      catch (Exception $e){
          echo "<pre>";
          echo print_r($response,true);
          echo "</pre>";
          echo $e->getMessage();
                 // print_r($data);    
          die('!!Error Occured!!');
      }
  }
  else{
     echo "No Update Required";
  }

}
function deleteData($db){
    global $options;
    global $totalRowsToIndex, $indexLimit;
    $limit = $indexLimit;
    $con = mssql_connect(DB_HOST,DB_USER,DB_PASS) or die('no'.mssql_error());
    $link = mssql_select_db($db,$con);
    //$sql = "SELECT id,Uid from solr_delete WHERE  `status` = 0 order by `id` asc limit ".$limit;
    $sql = "SELECT TOP 10000 Id,UID from tbSearch_DeletedUID WHERE Status = 0 and UID is not null and UID !='' order by Id asc";
    $query = mssql_query($sql) or die('Query 1 failed: ');
    $totalCnt = mssql_num_rows($query);
    $uids = $ids = array();
    $uids_str = '';
    $chunkSize =50;
    $i = 1;
    $j = 1;
    $updated_date = date('Y-m-d H:i:s');
    while($result = mssql_fetch_assoc($query)){
        $uids[$j][] = $result['UID'];
        $ids[$j][] = $result['Id'];
        $uids_str .= '"'.$result['UID'].'",';
        if(!empty($uids[$j]) && $i>=0 &&  ( $i/($chunkSize*$j)>=1 || ( $totalCnt==$i))){
           $uids_str = substr($uids_str,0,strlen($uids_str)-1);
           if(deleteByUids($uids[$j])){
                $sql_update = "UPDATE tbSearch_DeletedUID set Status = -1,ModifiedDate='".$updated_date."' where Id IN( ".implode(',', $ids[$j])." )";
                $delete_query = mssql_query($sql_update) or die('update query for query1 tbSearch_DeletedUID failed');
                $sql_data_update = "UPDATE tbSearch_FlatTable set Indexed = -1 where Uid IN( ".$uids_str." )";
                $delete_data_query = mssql_query($sql_data_update) or die('update query2 for tbSearch_FlatTable failed');
                if($delete_data_query){
                    echo '\n records with ids: '.implode(',', $ids[$j]).':  deleted and commited successfully';
                }
            }
            $uids_str = '';
            $j++;
            sleep(1);
        }
        $i++;
    }
echo "\n---------- Total: ".($i-1)." record deleted-------- \n";
    return true;
}
                        

//deleteAllDocuments(); die('deleted all records and commited');
//deleteData('BuddyCRMDB_001');
//deleteByUids(array('2','3'));

if (!empty($_GET["subs_id"]) && !empty($_GET["func"]) && $_GET["func"]== 'addData') {
    addData($_GET["subs_id"]);
}else if (!empty($_GET["subs_id"]) && !empty($_GET["func"]) && $_GET["func"]== 'deleteData') {
    deleteData($_GET["subs_id"]);
    //print('suhani');
}else if (!empty($_GET["subs_id"]) && !empty($_GET["func"]) && $_GET["func"]== 'deleteAllData') {
    deleteAllDocuments();
}else{
    $listDB = [];
    $listDB = getDBListFromCsv();

    if(!empty($listDB)){
        foreach($listDB as $dbName){
            addData($dbName[0]);
            echo '>>'.$dbName[0]. 'Scheduler Executed.' ;
        }
        exit("BuddyCrm Scheduler Executed Successfully");
    }
    echo '!!Error Occured!!' ; 
}
//addData('BuddyCRMDB_001');
//if (!empty($_GET["prefix"]) && !empty($_GET["func"])) {
//    AutoSuggest($_GET["prefix"]);
//}
//if (!empty($_GET["data_type"]) && !empty($_GET["val_string"])) {
//    MainQuery($_GET["data_type"],$_GET["val_string"]);
//}

//MainQuery();

/**
* returns array of db name.
*/
function getDBListFromCsv(){
    $file = fopen("C:/xamppNew/htdocs/solr_index/BuddyDB.csv","r");
    $dbNameList = [];

    while(! feof($file))
      {
      $dbNameList[] = fgetcsv($file);
      }

    fclose($file);
    return $dbNameList;
}
 

?>
