<?php

function deleteData($db){
    global $options;
    global $totalRowsToIndex, $indexLimit;
    $limit = $indexLimit;
    $con = mssql_connect(DB_HOST,DB_USER,DB_PASS) or die('no'.mssql_get_last_message());
	$link = mssql_select_db($db,$con);
	
    $sql = "SELECT TOP 1000 Id,CAST(UID AS TEXT) AS UID from tbSearch_DeletedUID WHERE Status = 0 and UID is not null and UID !='' order by Id asc";
	
	$query = mssql_query($sql) or die('Query 1 failed: '.mssql_get_last_message());
	
    $totalCnt = mssql_num_rows($query);
    $uids = $ids = array();
    $uids_str = '';
    $chunkSize =50;
    $i = 1;
    $j = 1;
    $updated_date = date('Y-m-d H:i:s');
    while($result = mssql_fetch_assoc($query)){
	    $uids[$j][] = $result['UID'];
        $ids[$j][] = $result['Id'];
        $uids_str .= '"'.$result['UID'].'",';
        if(!empty($uids[$j]) && $i>=0 &&  ( $i/($chunkSize*$j)>=1 || ( $totalCnt==$i))){
           $uids_str = substr($uids_str,0,strlen($uids_str)-1);
		   if(deleteByUids($uids[$j])){
		        $sql_update = "UPDATE tbSearch_DeletedUID set Status = -1,ModifiedDate='".$updated_date."' where Id IN( ".implode(',', $ids[$j])." )";
                $delete_query = mssql_query($sql_update) or die('update query for query1 tbSearch_DeletedUID failed'.mssql_get_last_message());				
                $sql_data_update = "UPDATE tbSearch_FlatTable set Indexed = -1 where Uid IN( ".$uids_str." )";
                $delete_data_query = mssql_query($sql_data_update) or die('update query2 for tbSearch_FlatTable failed');
                if($delete_data_query){
                    echo '\n records with ids: '.implode(',', $ids[$j]).':  deleted and commited successfully';
                }
            }
            $uids_str = '';
            $j++;
			sleep(1);
        }
        $i++;
    }
echo "\n---------- Total: ".($i-1)." record deleted-------- \n";
    return true;
}
 /*
  * Delete All Data For the Record
  */
 function deleteAllData($db){
    global $options;
    global $totalRowsToIndex, $indexLimit;
    $limit = $indexLimit;
    $con = mssql_connect(DB_HOST,DB_USER,DB_PASS) or die('no'.mssql_get_last_message());
    $link = mssql_select_db($db,$con);
    
    $sql = "SELECT TOP 1000 Id,CAST(UID AS TEXT) AS UID from tbSearch_DeletedUID WHERE Status = 0 and UID is not null and UID !='' order by Id asc";	
    
    $query = mssql_query($sql) or die('Query 1 failed: '.mssql_get_last_message());
    
    $totalCnt = mssql_num_rows($query);
    $uids = $ids = array();
    $uids_str = '';
    $chunkSize =50;
    $i = 1;
    $j = 1;
    $updated_date = date('Y-m-d H:i:s');
    while($result = mssql_fetch_assoc($query)){
		$recordUIDs = getAllUIDs($con,$result['UID']);
		if($recordUIDs && !empty($recordUIDs)){
			$arr = explode(',',str_replace('"','',$recordUIDs));
			$uids[$j] = $arr;
			$ids[$j][]  = getDeletedIDs($recordUIDs, $j);
			$uids_str .= $recordUIDs.",";
		} else if(!empty($uids[$j]) && in_array($result['UID'],$uids[$j])){
			$uids[$j][] = $result['UID'];
			$ids[$j][] = $result['Id'];
			$uids_str .= '"'.$result['UID'].'",';
		}
        if(!empty($uids[$j]) && $i>=0 &&  ( $i/($chunkSize*$j)>=1 || ( $totalCnt==$i))){
           $uids_str = substr($uids_str,0,strlen($uids_str)-1);
           if(deleteByUids($uids[$j])){
                $sql_update = "UPDATE tbSearch_DeletedUID set Status = -1,ModifiedDate='".$updated_date."' where Id IN( ".implode(',', $ids[$j])." )";
                $delete_query = mssql_query($sql_update) or die('update query for query1 tbSearch_DeletedUID failed'.mssql_get_last_message());             
                $sql_data_update = "UPDATE tbSearch_FlatTable set Indexed = -1 where Uid IN( ".$uids_str." )";
				$delete_data_query = mssql_query($sql_data_update) or die('update query2 for tbSearch_FlatTable failed');
                if($delete_data_query){
                    echo '\n records with ids: '.implode(',', $ids[$j]).':  deleted and commited successfully';
                }
            }
            $uids_str = '';
            $j++;
            sleep(1);
        }
        $i++;
    }
echo "\n---------- Total: ".($i-1)." record deleted-------- \n";
    return true;
}
/*
 *Checks For Record Data Type
 */
 function getAllUIDs($con, $uId){
	if(!$uId || !$con){
		return false;
	}
	$uIDs = '';
	$sql = 'SELECT CAST(Data_Type AS TEXT) AS Data_Type, Cid FROM tbSearch_FlatTable WHERE Uid = "'.$uId.'"';
	$query = mssql_query($sql) or die('Query 1 failed: '.mssql_get_last_message());
    $result = mssql_fetch_assoc($query);
	$dataType = $result['Data_Type'];
	$cId = $result['Cid'];
	if($dataType && $dataType == 'Record'){
		$sql1 = 'SELECT CAST(UID AS TEXT) AS UID FROM tbSearch_FlatTable WHERE Cid = '.$cId.'';
		$query1 = mssql_query($sql1) or die('Query 1 failed: '.mssql_get_last_message());
		$totalCnt = mssql_num_rows($query1);
		while($result1 = mssql_fetch_assoc($query1)){
			$uIDs .= '"'.$result1['UID'].'",';
		}
		$uIDs = rtrim($uIDs,',');
	}
	return $uIDs;
 }
 /*
  * Get Id From Delete Table
  */
  function getDeletedIDs($uIDs){
	$ids = array();
	$getIDSql = 'SELECT Id FROM tbSearch_DeletedUID WHERE UID IN ('.$uIDs.')';
	$getIDQuery = mssql_query($getIDSql) or die('Query 1 failed: '.mssql_get_last_message());
	$totalCnt = mssql_num_rows(getIDQuery);
	while($data = mssql_fetch_assoc($getIDQuery)){
		$ids[] = $data['Id'];
	}
	$ids = implode(',',$ids);
	return $ids;
  }
  
  //Deletion From Solr
  
  function deleteByUids($Uids){
    global $options;
    $client = new SolrClient($options);
    $res = false;
    try {
        if( !empty($Uids) && is_array($Uids)){
            $res = $client->deleteByIds($Uids);
        }
        else if(!empty($Uids)){
            $res = $client->deleteById($Uids);
        }
        if($res){
		echo 'in res';
            commitAll();
            return true;
        }
    }
    catch (Exception $e){
        echo '\n Exception'.$e->getMessage();
        return false;
    }
}
  
